terraform {
  required_version = ">= 0.13"
}

resource "random_pet" "name" {
  separator = "-"
}

locals {
  project = "terraform-test-project-280512"
  suffix  = "production-${random_pet.name.id}"
  region  = "europe-north1"
  zones   = ["europe-north1-a"]
}

provider "google" {
  version = "3.31.0"
  project = local.project
  region  = local.region
}

resource "google_project_service" "project" {
  for_each = var.google_apis
  service  = each.key

  disable_dependent_services = true
}

module "cloud-starter" {
  source  = "../../modules/services/cloud-starter"
  project = local.project
  suffix  = local.suffix
}

output "gcloud_command" {
  value = module.cloud-starter.gcloud_command
}
