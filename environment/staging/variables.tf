variable "google_apis" {
  type = set(string)
  default = [
    "iam.googleapis.com",
    "container.googleapis.com"
  ]
}
