
resource "google_compute_network" "vpc_network" {
  name                    = "vpc-network-${var.suffix}"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "kubernetes_subnetwork" {
  name          = "kubernetes-subnetwork-${var.suffix}"
  ip_cidr_range = "10.2.0.0/16"
  region        = var.region
  network       = google_compute_network.vpc_network.id
  secondary_ip_range {
    range_name    = "gke-pods"
    ip_cidr_range = "192.168.10.0/24"
  }
  secondary_ip_range {
    range_name    = "gke-services"
    ip_cidr_range = "192.168.20.0/24"
  }
}

module "gke" {
  source     = "terraform-google-modules/kubernetes-engine/google"
  project_id = var.project
  name       = "kubernetes-${var.suffix}"
  region     = var.region
  zones      = var.zones
  network    = google_compute_network.vpc_network.name
  subnetwork = google_compute_subnetwork.kubernetes_subnetwork.name

  ip_range_pods          = google_compute_subnetwork.kubernetes_subnetwork.secondary_ip_range[0].range_name
  ip_range_services      = google_compute_subnetwork.kubernetes_subnetwork.secondary_ip_range[1].range_name
  create_service_account = true
  grant_registry_access  = true
}
