variable "region" {
  type    = string
  default = "europe-north1"
}

variable "project" {
  type = string
}

variable "zones" {
  type    = set(string)
  default = ["europe-north1-a"]
}

variable "suffix" {
  type = string
}
