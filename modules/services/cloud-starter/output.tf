output "gcloud_command" {
  value = "gcloud container clusters get-credentials ${module.gke.name} --region ${var.region} --project ${var.project}"
}
